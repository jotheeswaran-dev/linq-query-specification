namespace LinqQuerySpecification.Tests.Filters
{
  using System.Collections.Generic;
  using System.Linq;
  using LinqQuerySpecification.Filters;
  using Xunit;

  public class StringFilterTests
  {
    private class ClassToTestString
    {
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
      public string? Name { get; set; }
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    }

    [Fact]
    public void ShouldReturnResultForEqualTo()
    {
      var testData = GetTestData();

      var result = new StringFilter { EqualTo = "Alexandra" }
        .Apply(testData.AsQueryable(), nameof(ClassToTestString.Name));

      Assert.Equal(1, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1] || x == testData[2] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForNotEqualTo()
    {
      var testData = GetTestData();

      var result = new StringFilter { NotEqualTo = "Alexandra" }
        .Apply(testData.AsQueryable(), nameof(ClassToTestString.Name));

      Assert.Equal(3, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1] || x == testData[2] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnValuesWithSpecifiedFilter()
    {
      var testData = new List<ClassToTestString> { new ClassToTestString { Name = "something" }, new ClassToTestString() };

      var result = new BooleanFilter { Specified = false }
        .Apply(testData.AsQueryable(), nameof(ClassToTestString.Name));

      Assert.Equal(1, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1]);
    }

    [Fact]
    public void ShouldReturnResultForIn()
    {
      var testData = GetTestData();

      var result = new StringFilter { In = new[] { "In Test", "non-existent entry" } }
        .Apply(testData.AsQueryable(), nameof(ClassToTestString.Name));

      Assert.Equal(1, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0] || x == testData[1] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForContains()
    {
      var testData = GetTestData();

      var result = new StringFilter { Contains = "test" }
        .Apply(testData.AsQueryable(), nameof(ClassToTestString.Name));

      Assert.Equal(2, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[1] || x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForDoesNotContain()
    {
      var testData = GetTestData();

      var result = new StringFilter { DoesNotContain = "Not" }
        .Apply(testData.AsQueryable(), nameof(ClassToTestString.Name));

      Assert.Equal(3, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[2] || x == testData[3]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1]);
    }

    private static List<ClassToTestString> GetTestData() =>
      new List<ClassToTestString>
      {
        new ClassToTestString { Name = "Alexandra" },
        new ClassToTestString { Name = "Not Test" },
        new ClassToTestString { Name = "In Test" },
        new ClassToTestString { Name = "Amelia" },
      };
  }
}
