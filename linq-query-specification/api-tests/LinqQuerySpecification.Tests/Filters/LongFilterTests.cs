namespace LinqQuerySpecification.Tests.Filters
{
  using System.Collections.Generic;
  using System.Linq;
  using LinqQuerySpecification.Filters;
  using Xunit;

  public class LongFilterTests
  {
    private class ClassToTestLong
    {
      public long? Id { get; set; }
    }

    [Fact]
    public void ShouldReturnResultForEqualTo()
    {
      var testData = GetTestData();

      var result = new LongFilter { EqualTo = 1 }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(1, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1] || x == testData[2] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForNotEqualTo()
    {
      var testData = GetTestData();

      var result = new LongFilter { NotEqualTo = 1 }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(3, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1] || x == testData[2] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnValuesWithSpecifiedFilter()
    {
      var testData = new List<ClassToTestLong> { new ClassToTestLong { Id = 1 }, new ClassToTestLong() };

      var result = new BooleanFilter { Specified = false }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(1, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1]);
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnResultForIn()
    {
      var testData = GetTestData();

      var result = new LongFilter { In = new long?[] { 1, 2, 3, 4, 5 } }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(2, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForGreaterThan()
    {
      var testData = GetTestData();

      var result = new LongFilter { GreaterThan = 1 }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(1, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0] || x == testData[1] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForGreaterThanOrEqual()
    {
      var testData = GetTestData();

      var result = new LongFilter { GreaterThanOrEqual = 1 }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(2, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1] || x == testData[3]);
    }

    [Fact]
    public void ShouldReturnResultForLessThan()
    {
      var testData = GetTestData();

      var result = new LongFilter { LessThan = 2 }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(3, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[1] || x == testData[3]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[2]);
    }

    [Fact]
    public void ShouldReturnResultForLessThanOrEqual()
    {
      var testData = GetTestData();

      var result = new LongFilter { LessThanOrEqual = 1 }
        .Apply(testData.AsQueryable(), nameof(ClassToTestLong.Id));

      Assert.Equal(3, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[1] || x == testData[3]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[2]);
    }

    private static List<ClassToTestLong> GetTestData() =>
      new List<ClassToTestLong>
      {
        new ClassToTestLong { Id = 1 },
        new ClassToTestLong { Id = -5 },
        new ClassToTestLong { Id = 5 },
        new ClassToTestLong { Id = -7 },
      };
  }
}
