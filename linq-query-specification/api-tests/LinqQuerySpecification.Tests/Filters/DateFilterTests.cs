namespace LinqQuerySpecification.Tests.Filters
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using LinqQuerySpecification.Filters;
  using Xunit;

  public class DateFilterTests
  {
    private class ClassToTestDate
    {
      public DateTime? StartDate { get; set; }
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValueForEqualToDateFilter()
    {
      var testData = GetTestData();

      var result = new DateFilter { EqualTo = new DateTime(2020, 12, 30) }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(1, result.Count());
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValueForNotEqualToDateFilter()
    {
      var testData = GetTestData();

      var result = new DateFilter { NotEqualTo = new DateTime(2020, 12, 30) }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(1, result.Count());
    }

    [Fact]
    public void ShouldReturnValuesWithSpecifiedFilter()
    {
      var testData = new List<ClassToTestDate> { new ClassToTestDate { StartDate = new DateTime(2020, 1, 1) }, new ClassToTestDate() };

      var result = new BooleanFilter { Specified = false }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(1, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1]);
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnResultForIn()
    {
      var testData = GetTestData();

      var result = new DateFilter { In = new DateTime?[] { new DateTime(2020, 12, 30) } }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(2, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1] || x == testData[3]);
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValueForGreaterThanDateFilter()
    {
      var testData = GetTestData();

      var result = new DateFilter { GreaterThan = new DateTime(2020, 12, 30) }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(2, result.Count());
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValueForGreaterThanOrEqualDateFilter()
    {
      var testData = GetTestData();

      var result = new DateFilter { GreaterThanOrEqual = new DateTime(2020, 12, 30) }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(2, result.Count());
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValueForLessThanDateFilter()
    {
      var testData = GetTestData();

      var result = new DateFilter { LessThan = new DateTime(2020, 12, 30) }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(2, result.Count());
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValueForLessThanOrEqualDateFilter()
    {
      var testData = GetTestData();

      var result = new DateFilter { LessThanOrEqual = new DateTime(2020, 12, 30) }
        .Apply(testData.AsQueryable(), nameof(ClassToTestDate.StartDate));

      Assert.Equal(2, result.Count());
    }

    private static List<ClassToTestDate> GetTestData() => new List<ClassToTestDate>
    {
      new ClassToTestDate { StartDate = new DateTime(2020, 12, 30) },
      new ClassToTestDate { StartDate = new DateTime(2021, 1, 30) },
      new ClassToTestDate { StartDate = new DateTime(2022, 1, 30) },
    };
  }
}
