namespace LinqQuerySpecification.Filters
{
  using System;
  using System.Linq;
  using System.Linq.Expressions;
  using System.Reflection;

  /// <summary>
  ///   Class for filtering attributes with <see cref="DateTime" /> type.
  /// </summary>
  /// <remarks>
  ///   It can be added to a criteria class as a member, to support the following query parameters:
  ///   <code>
  ///    fieldName.EqualTo = 2020-01-01
  ///    fieldName.NotEqualTo = 2020-01-01
  ///    fieldName.Specified = true  // or false
  ///    fieldName.In = 2020-01-01, 2020-04-15
  ///    fieldName.GreaterThan = 2020-01-01
  ///    fieldName.GreaterThanOrEqual = 2020-01-01
  ///    fieldName.LessThan = 2020-01-01
  ///    fieldName.LessThanOrEqual = 2020-01-01
  ///  </code>
  /// </remarks>
  public class DateFilter : RangeFilter<DateTime?>
  {
    private static PropertyInfo DatePropertyFromDatetimeType => typeof(DateTime?).GetProperty("Date");

    /// <summary>
    ///   Applies the filter logic looping through each of the filters that have been set.
    /// </summary>
    /// <remarks>
    ///   Applies the sql-AND condition across each of the specified filters.
    /// </remarks>
    /// <typeparam name="T">The type of the field to which this instance of <c>Filter</c> is applicable to.</typeparam>
    /// <param name="queryable">The pre-existing queryable instance.</param>
    /// <param name="propertyName">The name of the property of <typeparamref name="T" /> for which the filter conditions needs to be applied to.</param>
    /// <returns>The <paramref name="queryable" /> with new linq expressions added.</returns>
    public new IQueryable<T> Apply<T>(IQueryable<T> queryable, string propertyName)
    {
      if (queryable.Any())
      {
        var linqFunctionParameter = Expression.Parameter(typeof(T));
        var modelProperty = Expression.Property(linqFunctionParameter, propertyName);

        queryable = ApplyEqualTo(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyNotEqualTo(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplySpecified(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyIn(queryable);
        queryable = ApplyGreaterThanOrEqual(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyGreaterThan(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyLessThanOrEqual(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyLessThan(queryable, linqFunctionParameter, modelProperty);
      }

      return queryable;
    }

    private IQueryable<T> ApplyIn<T>(IQueryable<T> queryable)
    {
      // TODO: Need to implement "ApplyIn" logic
      if (In != null && In.Any(x => x != null))
      {
        In = In.Where(x => x != null).ToList();
      }

      return queryable;
    }

    private IQueryable<T> ApplySpecified<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (Specified != null)
      {
        var nullValue = Expression.Constant(null);
        var converted = Expression.Convert(modelProperty, typeof(DateTime?));
        var operationBody = Specified == true ? Expression.NotEqual(converted, nullValue) : Expression.Equal(converted, nullValue);
        queryable = queryable.Where(Expression.Lambda<Func<T, bool>>(operationBody, linqFunctionParameter));
      }

      return queryable;
    }

    private IQueryable<T> ApplyLessThan<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (LessThan != null)
      {
        var leftExpression = Expression.MakeMemberAccess(modelProperty, DatePropertyFromDatetimeType);
        var rightExpression = Expression.MakeMemberAccess(Expression.Constant(LessThan), DatePropertyFromDatetimeType);
        var comparison = Expression.LessThan(leftExpression, rightExpression);
        queryable = AddWhere(queryable, linqFunctionParameter, comparison);
      }

      return queryable;
    }

    private IQueryable<T> ApplyLessThanOrEqual<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (LessThanOrEqual != null)
      {
        var leftExpression = Expression.MakeMemberAccess(modelProperty, DatePropertyFromDatetimeType);
        var rightExpression = Expression.MakeMemberAccess(Expression.Constant(LessThanOrEqual), DatePropertyFromDatetimeType);
        var comparison = Expression.LessThanOrEqual(leftExpression, rightExpression);
        queryable = AddWhere(queryable, linqFunctionParameter, comparison);
      }

      return queryable;
    }

    private IQueryable<T> ApplyGreaterThan<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (GreaterThan != null)
      {
        var leftExpression = Expression.MakeMemberAccess(modelProperty, DatePropertyFromDatetimeType);
        var rightExpression = Expression.MakeMemberAccess(Expression.Constant(GreaterThan), DatePropertyFromDatetimeType);
        var comparison = Expression.GreaterThan(leftExpression, rightExpression);
        queryable = AddWhere(queryable, linqFunctionParameter, comparison);
      }

      return queryable;
    }

    private IQueryable<T> ApplyGreaterThanOrEqual<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (GreaterThanOrEqual != null)
      {
        var leftExpression = Expression.MakeMemberAccess(modelProperty, DatePropertyFromDatetimeType);
        var rightExpression = Expression.MakeMemberAccess(Expression.Constant(GreaterThanOrEqual), DatePropertyFromDatetimeType);
        var comparison = Expression.GreaterThanOrEqual(leftExpression, rightExpression);
        queryable = AddWhere(queryable, linqFunctionParameter, comparison);
      }

      return queryable;
    }

    private IQueryable<T> ApplyNotEqualTo<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (NotEqualTo != null)
      {
        var leftExpression = Expression.MakeMemberAccess(modelProperty, DatePropertyFromDatetimeType);
        var rightExpression = Expression.MakeMemberAccess(Expression.Constant(NotEqualTo), DatePropertyFromDatetimeType);
        var comparison = Expression.NotEqual(leftExpression, rightExpression);
        queryable = AddWhere(queryable, linqFunctionParameter, comparison);
      }

      return queryable;
    }

    private IQueryable<T> ApplyEqualTo<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (EqualTo != null)
      {
        var leftExpression = Expression.MakeMemberAccess(modelProperty, DatePropertyFromDatetimeType);
        var rightExpression = Expression.MakeMemberAccess(Expression.Constant(EqualTo), DatePropertyFromDatetimeType);
        var comparison = Expression.Equal(leftExpression, rightExpression);
        queryable = AddWhere(queryable, linqFunctionParameter, comparison);
      }

      return queryable;
    }
  }
}
