namespace LinqQuerySpecification.Filters
{
  using System;
  using System.Diagnostics.CodeAnalysis;
  using System.Linq;
  using System.Linq.Expressions;
  using LinqQuerySpecification.Utils;

  /// <summary>
  ///   Abstract filter class for Comparable types, where less than / greater than / etc relations could be interpreted.
  /// </summary>
  /// <remarks>
  ///   It can be added to a criteria class as a member, to support the following query parameters:
  ///   <code>
  ///    fieldName.EqualTo = 42
  ///    fieldName.NotEqualTo = 42
  ///    fieldName.Specified = true  // or false
  ///    fieldName.In = 43, 42
  ///    fieldName.GreaterThan = 41
  ///    fieldName.GreaterThanOrEqual = 42
  ///    fieldName.LessThan = 44
  ///    fieldName.LessThanOrEqual = 44
  ///  </code>
  ///   Due to problems with the type conversions, the descendant classes should be used, where the generic type parameter
  ///   is materialized.
  /// </remarks>
  /// <typeparam name="TFieldType">
  ///   The field type for which this <c>Filter</c> applies.
  /// </typeparam>
  /// <seealso cref="DateFilter" />
  /// <seealso cref="LongFilter" />
  public abstract class RangeFilter<TFieldType> : Filter<TFieldType>
  {
    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the 'greater than' matching needs to happen.
    /// </summary>
    public TFieldType GreaterThan { get; set; }

    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the 'greater than or equal to' matching needs to happen.
    /// </summary>
    public TFieldType GreaterThanOrEqual { get; set; }

    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the 'less than' matching needs to happen.
    /// </summary>
    public TFieldType LessThan { get; set; }

    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the 'less than or equal to' matching needs to happen.
    /// </summary>
    public TFieldType LessThanOrEqual { get; set; }

    /// <summary>
    ///   Applies the filter logic looping through each of the filters that have been set.
    /// </summary>
    /// <remarks>
    ///   Applies the sql-AND condition across each of the specified filters.
    /// </remarks>
    /// <typeparam name="T">The type of the field to which this instance of <c>Filter</c> is applicable to.</typeparam>
    /// <param name="queryable">The pre-existing queryable instance.</param>
    /// <param name="propertyName">The name of the property of <typeparamref name="T" /> for which the filter conditions needs to be applied to.</param>
    /// <returns>The <paramref name="queryable" /> with new linq expressions added.</returns>
    public new IQueryable<T> Apply<T>(IQueryable<T> queryable, string propertyName)
    {
      queryable = base.Apply(queryable, propertyName);

      if (queryable.Any())
      {
        var linqFunctionParameter = Expression.Parameter(typeof(T));
        var modelProperty = Expression.Property(linqFunctionParameter, propertyName);

        queryable = ApplyGreaterThanOrEqual(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyGreaterThan(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyLessThanOrEqual(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyLessThan(queryable, linqFunctionParameter, modelProperty);
      }

      return queryable;
    }

    /// <summary>Determines whether the specified object is equal to the current object.</summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>
    ///   <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />.
    /// </returns>
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    [ExcludeFromCodeCoverage]
    public override bool Equals(object? obj)
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    {
      if (this == obj)
      {
        return true;
      }

      if (obj == null || GetType() != obj.GetType())
      {
        return false;
      }

      if (!base.Equals(obj))
      {
        return false;
      }

      var that = (RangeFilter<TFieldType>)obj;
      return Objects.AreEqual(GreaterThan, that.GreaterThan) &&
             Objects.AreEqual(GreaterThanOrEqual, that.GreaterThanOrEqual) &&
             Objects.AreEqual(LessThan, that.LessThan) &&
             Objects.AreEqual(LessThanOrEqual, that.LessThanOrEqual);
    }

    /// <summary>Serves as the default hash function taking into account all properties on this class.</summary>
    /// <returns>A hash code for the current object.</returns>
    [ExcludeFromCodeCoverage]
    public override int GetHashCode() => HashCode.Combine(base.GetHashCode(), GreaterThan, GreaterThanOrEqual, LessThan, LessThanOrEqual);

    /// <summary>Returns a string that represents the current object.</summary>
    /// <returns>A string that represents the current object.</returns>
    [ExcludeFromCodeCoverage]
    public override string ToString() => GetFilterName() + " ["
                                                         + (EqualTo != null ? "EqualTo=" + EqualTo + ", " : string.Empty)
                                                         + (NotEqualTo != null ? "NotEqualTo=" + NotEqualTo + ", " : string.Empty)
                                                         + (Specified != null ? "Specified=" + Specified : string.Empty)
                                                         + (In != null ? "In=" + In : string.Empty)
                                                         + (GreaterThan != null ? "GreaterThan=" + GreaterThan + ", " : string.Empty)
                                                         + (GreaterThanOrEqual != null ? "GreaterThanOrEqual=" + GreaterThanOrEqual + ", " : string.Empty)
                                                         + (LessThan != null ? "LessThan=" + LessThan + ", " : string.Empty)
                                                         + (LessThanOrEqual != null ? "LessThanOrEqual=" + LessThanOrEqual + ", " : string.Empty)
                                                         + "]";

    private IQueryable<T> ApplyLessThanOrEqual<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (LessThanOrEqual != null)
      {
        var typeFilter = Expression.Convert(GetOperand(LessThanOrEqual), modelProperty.Type);
        var operationBody = Expression.LessThanOrEqual(modelProperty, typeFilter);
        queryable = AddWhere(queryable, linqFunctionParameter, operationBody);
      }

      return queryable;
    }

    private IQueryable<T> ApplyLessThan<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (LessThan != null)
      {
        var typeFilter = Expression.Convert(GetOperand(LessThan), modelProperty.Type);
        var operationBody = Expression.LessThan(modelProperty, typeFilter);
        queryable = AddWhere(queryable, linqFunctionParameter, operationBody);
      }

      return queryable;
    }

    private IQueryable<T> ApplyGreaterThanOrEqual<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (GreaterThanOrEqual != null)
      {
        var typeFilter = Expression.Convert(GetOperand(GreaterThanOrEqual), modelProperty.Type);
        var operationBody = Expression.GreaterThanOrEqual(modelProperty, typeFilter);
        queryable = AddWhere(queryable, linqFunctionParameter, operationBody);
      }

      return queryable;
    }

    private IQueryable<T> ApplyGreaterThan<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (GreaterThan != null)
      {
        var typeFilter = Expression.Convert(GetOperand(GreaterThan), modelProperty.Type);
        var operationBody = Expression.GreaterThan(modelProperty, typeFilter);
        queryable = AddWhere(queryable, linqFunctionParameter, operationBody);
      }

      return queryable;
    }
  }
}
