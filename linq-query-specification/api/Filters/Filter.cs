namespace LinqQuerySpecification.Filters
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics.CodeAnalysis;
  using System.Linq;
  using System.Linq.Expressions;
  using LinqQuerySpecification.Utils;

  /// <summary>
  ///   Base class for filtering attributes. It can be added to a criteria class as a member, to support the following query parameters:
  ///   <code>
  ///    fieldName.EqualTo = 'something'
  ///    fieldName.NotEqualTo = 'somethingElse'
  ///    fieldName.Specified = true  // or false
  ///    fieldName.In = 'something', 'other'
  ///  </code>
  /// </summary>
  /// <remarks>
  ///   Specifying more than 1 condition for the same Filter class should result in an 'AND' [sql where] condition.
  ///   To specify a sql 'OR' condition for the same field, you should use the 'In' clause.
  /// </remarks>
  /// <typeparam name="TFieldType">The field type for which this <c>Filter</c> applies.</typeparam>
  public abstract class Filter<TFieldType>
  {
    private readonly CaseInsensitiveExpressionVisitor caseInsensitiveExpressionVisitor;

    /// <summary>Initializes a new instance of the <see cref="Filter{TFieldType}" /> class that is empty.</summary>
    protected Filter() => caseInsensitiveExpressionVisitor = new CaseInsensitiveExpressionVisitor();

    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the case-insensitive matching needs to happen.
    /// </summary>
    public TFieldType EqualTo { get; set; }

    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the case-insensitive negative matching needs to happen.
    /// </summary>
    public TFieldType NotEqualTo { get; set; }

    /// <summary>
    ///   Gets or sets the property to depict whether the field has a <see langword="null" /> value in the DB or not.
    /// </summary>
    public bool? Specified { get; set; }

    /// <summary>
    ///   Gets or sets the property to hold the list of values of the field against which the matching needs to happen.
    /// </summary>
    public IEnumerable<TFieldType> In { get; set; }

    /// <summary>
    ///   Applies the filter logic looping through each of the filters that have been set.
    /// </summary>
    /// <remarks>
    ///   Applies the sql-AND condition across each of the specified filters.
    /// </remarks>
    /// <typeparam name="T">The type of the field to which this instance of <c>Filter</c> is applicable to.</typeparam>
    /// <param name="queryable">The pre-existing queryable instance.</param>
    /// <param name="propertyName">The name of the property of <typeparamref name="T" /> for which the filter conditions needs to be applied to.</param>
    /// <returns>The <paramref name="queryable" /> with new linq expressions added.</returns>
    public IQueryable<T> Apply<T>(IQueryable<T> queryable, string propertyName)
    {
      if (queryable.Any())
      {
        var linqFunctionParameter = Expression.Parameter(typeof(T));
        var modelProperty = Expression.Property(linqFunctionParameter, propertyName);

        queryable = ApplyEqualTo(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyNotEqualTo(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplySpecified(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyIn(queryable, linqFunctionParameter, modelProperty);
      }

      return queryable;
    }

    /// <summary>Determines whether the specified object is equal to the current object.</summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>
    ///   <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />.
    /// </returns>
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    [ExcludeFromCodeCoverage]
    public override bool Equals(object? obj)
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    {
      if (this == obj)
      {
        return true;
      }

      if (obj == null || GetType() != obj.GetType())
      {
        return false;
      }

      var filter = (Filter<TFieldType>)obj;
      return Objects.AreEqual(EqualTo, filter.EqualTo) &&
             Objects.AreEqual(NotEqualTo, filter.NotEqualTo) &&
             Objects.AreEqual(Specified, filter.Specified) &&
             Objects.AreEqual(In, filter.In);
    }

    /// <summary>Serves as the default hash function taking into account all properties on this class.</summary>
    /// <returns>A hash code for the current object.</returns>
    [ExcludeFromCodeCoverage]
    public override int GetHashCode() => HashCode.Combine(EqualTo, NotEqualTo, Specified, In);

    /// <summary>Returns a string that represents the current object.</summary>
    /// <returns>A string that represents the current object.</returns>
    [ExcludeFromCodeCoverage]
    public override string ToString() => GetFilterName() + " ["
                                                         + (EqualTo != null ? "EqualTo=" + EqualTo + ", " : string.Empty)
                                                         + (NotEqualTo != null ? "NotEqualTo=" + NotEqualTo + ", " : string.Empty)
                                                         + (Specified != null ? "Specified=" + Specified : string.Empty)
                                                         + (In != null ? "In=" + In : string.Empty)
                                                         + "]";

    /// <summary>
    ///   Builds the case-insensitive <c>Expression</c> of <paramref name="operationBody" />
    ///   and applies it as a 'where' clause on <paramref name="queryable" />.
    /// </summary>
    /// <typeparam name="T">The type of the field to which this instance of <c>Filter</c> is applicable to.</typeparam>
    /// <param name="queryable">The pre-existing queryable instance.</param>
    /// <param name="linqFunctionParameter">The linq function parameter.</param>
    /// <param name="operationBody">The linq operation to be performed.</param>
    /// <returns>The <paramref name="queryable" /> with new linq expressions added.</returns>
    protected IQueryable<T> AddWhere<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression operationBody)
    {
      if (operationBody == null)
      {
        return queryable;
      }

      var caseInsensitiveOperationBody = caseInsensitiveExpressionVisitor.Visit(operationBody);
      return queryable.Where(Expression.Lambda<Func<T, bool>>(caseInsensitiveOperationBody, linqFunctionParameter));
    }

    /// <summary>
    ///   Converts the <paramref name="field" /> parameter if its a <c>string</c> instance to lowercase. This is needed to do case-insensitive comparisons for <see cref="Filter{TFieldType}.ApplyEqualTo{T}(IQueryable{T},ParameterExpression,Expression)" /> and <see cref="Filter{TFieldType}.ApplyNotEqualTo{T}(IQueryable{T},ParameterExpression,Expression)" />.
    /// </summary>
    /// <param name="field">The param to try to convert to lowercase.</param>
    /// <returns>Teh converted <paramref name="field" />.</returns>
    protected TFieldType TryConvertToStringLower(TFieldType field)
    {
      if (field != null && field.GetType().IsAssignableFrom(typeof(string)))
      {
        // NOTE: Explicitly converted to String to run ToLower() method
#pragma warning disable CA1305 // Specify IFormatProvider
        var tempString = Convert.ChangeType(field, typeof(string));
#pragma warning restore CA1305 // Specify IFormatProvider
        tempString = tempString.ToString().ToLowerInvariant();
        return (TFieldType)tempString;
      }

      return field;
    }

    /// <summary>
    ///   Gets the name of the Type of the current object instance.
    /// </summary>
    /// <returns>The string name of the Type.</returns>
    [ExcludeFromCodeCoverage]
    protected string GetFilterName() => GetType().ToString();

    private protected static ConstantExpression GetOperand(TFieldType value) => Expression.Constant(value, typeof(TFieldType));

    private IQueryable<T> ApplyEqualTo<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (EqualTo != null)
      {
        EqualTo = TryConvertToStringLower(EqualTo);
        var typeFilter = Expression.Convert(GetOperand(EqualTo), modelProperty.Type);
        var operationBody = Expression.Equal(modelProperty, typeFilter);
        queryable = AddWhere(queryable, linqFunctionParameter, operationBody);
      }

      return queryable;
    }

    private IQueryable<T> ApplyNotEqualTo<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (NotEqualTo != null)
      {
        NotEqualTo = TryConvertToStringLower(NotEqualTo);
        var typeFilter = Expression.Convert(GetOperand(NotEqualTo), modelProperty.Type);
        var operationBody = Expression.NotEqual(modelProperty, typeFilter);
        queryable = AddWhere(queryable, linqFunctionParameter, operationBody);
      }

      return queryable;
    }

    private IQueryable<T> ApplySpecified<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (Specified != null)
      {
        var nullValue = Expression.Constant(null);
        var operationBody = Specified == true ? Expression.NotEqual(modelProperty, nullValue) : Expression.Equal(modelProperty, nullValue);
        queryable = queryable.Where(Expression.Lambda<Func<T, bool>>(operationBody, linqFunctionParameter));
      }

      return queryable;
    }

    private IQueryable<T> ApplyIn<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, Expression modelProperty)
    {
      if (In != null && In.Any(x => x != null))
      {
        In = In.Where(x => x != null).ToList();
        var listType = In.GetType().GetGenericArguments()[0];

        // HACKTAG: Explicitly handled most of the possible Numeric cases. Should refactor to make it in a better reusable pattern
        MethodCallExpression expr = null;
        if (listType == typeof(string))
        {
          var convertedQueryable = In.AsEnumerable().Select(TryConvertToStringLower).ToList();
          var method = typeof(List<TFieldType>).GetMethod("Contains", new[] { typeof(TFieldType) });
          expr = Expression.Call(Expression.Constant(convertedQueryable), method, modelProperty);
        }
        else if (listType == typeof(long?) || listType == typeof(long))
        {
          if (modelProperty.Type == typeof(long?))
          {
            expr = BuildNumericMethodCall<long?>(modelProperty);
          }
          else if (modelProperty.Type == typeof(long))
          {
            expr = BuildNumericMethodCall<long>(modelProperty);
          }
        }
        else if (listType == typeof(int?) || listType == typeof(int))
        {
          if (modelProperty.Type == typeof(int?))
          {
            expr = BuildNumericMethodCall<int?>(modelProperty);
          }
          else if (modelProperty.Type == typeof(int))
          {
            expr = BuildNumericMethodCall<int>(modelProperty);
          }
        }
        else if (listType == typeof(decimal?) || listType == typeof(decimal))
        {
          if (modelProperty.Type == typeof(decimal?))
          {
            expr = BuildNumericMethodCall<decimal?>(modelProperty);
          }
          else if (modelProperty.Type == typeof(decimal))
          {
            expr = BuildNumericMethodCall<decimal>(modelProperty);
          }
        }
        else if (listType == typeof(float?) || listType == typeof(float))
        {
          if (modelProperty.Type == typeof(float?))
          {
            expr = BuildNumericMethodCall<float?>(modelProperty);
          }
          else if (modelProperty.Type == typeof(float))
          {
            expr = BuildNumericMethodCall<float>(modelProperty);
          }
        }
        else if (listType == typeof(double?) || listType == typeof(double))
        {
          if (modelProperty.Type == typeof(double?))
          {
            expr = BuildNumericMethodCall<double?>(modelProperty);
          }
          else if (modelProperty.Type == typeof(double))
          {
            expr = BuildNumericMethodCall<double>(modelProperty);
          }
        }
        else if (listType == typeof(bool?) || listType == typeof(bool))
        {
          if (modelProperty.Type == typeof(bool?))
          {
            expr = BuildNumericMethodCall<bool?>(modelProperty);
          }
          else if (modelProperty.Type == typeof(bool))
          {
            expr = BuildNumericMethodCall<bool>(modelProperty);
          }
        }

        queryable = AddWhere(queryable, linqFunctionParameter, expr);
      }

      return queryable;
    }

    private MethodCallExpression BuildNumericMethodCall<T>(Expression modelProperty)
    {
      // Explicitly handling numeric types due to Nullable error when querying against non-nullable fields
      var convertedQueryable = In.Select(item =>
      {
        var convertedItem = Expression.Convert(GetOperand(item), modelProperty.Type);
        return Expression.Lambda<Func<T>>(convertedItem).Compile()();
      });

      var method = typeof(List<T>).GetMethod("Contains", new[] { typeof(T) });
      return Expression.Call(Expression.Constant(convertedQueryable), method, modelProperty);
    }
  }
}
