namespace LinqQuerySpecification.Utils
{
  using System;
  using System.Linq.Expressions;

  /// <summary>
  ///   A visitor to create an <see cref="Expression" /> for the specified <see cref="MemberExpression" />
  ///   if it is a <see cref="string" /> that converts it to a lower-case string.
  /// </summary>
  public class CaseInsensitiveExpressionVisitor : ExpressionVisitor
  {
    /// <summary>
    ///   Creates an <see cref="Expression" /> for the specified <paramref name="node" /> if it is a <see cref="string" />
    ///   that converts it to a lower-case string.
    /// </summary>
    /// <param name="node">The <see cref="MemberExpression" /> which has to be converted to a lower-case string.</param>
    /// <returns>A new <see cref="Expression" /> or the base return value.</returns>
    protected override Expression VisitMember(MemberExpression node)
    {
      if (node.Type == typeof(string))
      {
        var methodInfo = typeof(string).GetMethod("ToLower", Array.Empty<Type>());
        return Expression.Call(node, methodInfo);
      }

      return base.VisitMember(node);
    }
  }
}
