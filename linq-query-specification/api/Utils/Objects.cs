namespace LinqQuerySpecification.Utils
{
  /// <summary>
  ///   Utility class for determining whether 2 objects are equal.
  /// </summary>
  public static class Objects
  {
    /// <summary>
    ///   Checks whether the two objects are <see cref="Objects.AreEqual(object, object)" /> in a null-safe manner.
    /// </summary>
    /// <param name="a">The first object that needs to be checked.</param>
    /// <param name="b">The second object that needs to be checked.</param>
    /// <returns><see cref="bool" /> representing whether the objects are equal.</returns>
    public static bool AreEqual(object a, object b) => a == b || (a == null && b == null) || (a != null && b != null && a.Equals(b));
  }
}
