#!/usr/bin/env sh

# Note: Ignore failure code if tool is already present
dotnet tool list -g | GREP_OPTIONS= grep dotnet-reportgenerator-globaltool &> /dev/null || dotnet tool install -g dotnet-reportgenerator-globaltool

./test-dotnet-without-report.sh && $HOME/.dotnet/tools/reportgenerator "-reports:**/TestResults/Coverage/coverage.cobertura.xml" "-targetdir:$PWD/TestResults/Coverage/Reports" -reportTypes:Html
# Commenting out so that we can try to check if this report generation works in gitlab ci
# && open "$PWD/TestResults/Coverage/Reports/index.html"
