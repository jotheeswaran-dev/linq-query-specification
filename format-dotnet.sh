#!/usr/bin/env sh

# Note: Ignore failure code if tool is already present
dotnet tool list -g | GREP_OPTIONS= grep dotnet-format &> /dev/null || dotnet tool install -g dotnet-format

dotnet format
